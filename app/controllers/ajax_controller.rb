class AjaxController < ApplicationController
  def index
    @count = current_count
    respond_to do |format|
      format.html
    end
  end

  def counter
    session[:count] = current_count + 1
    respond_to do |format|
      format.json { render :json => {"count" => session[:count]} }
    end
  end

  private
  def current_count
    session[:count] ||= 0
  end
end
