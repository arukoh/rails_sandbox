# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$(($)->
  $('#counter-link')
    .live("ajax:success", (event, data, status, xhr)->
      $('#counter').empty().append(data.count)
    )
    .live("ajax:error", (data, status, xhr)->
      alert("failure!!!")
    )
)
