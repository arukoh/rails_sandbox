require 'spec_helper'

describe "routing to ajax" do

  it "routes /ajax to ajax#index" do
    expect(:get => "/ajax").to route_to(
      :controller => "ajax",
      :action => "index"
    )
  end

  it "routes /ajax/counter to ajax#counter" do
    expect(:get => "/ajax/counter").to route_to(
      :controller => "ajax",
      :action => "counter"
    )
  end

  #example: not to be routable
  #it "does not expose a list of profiles" do
  #  expect(:get => "/profiles").not_to be_routable
  #end

end
