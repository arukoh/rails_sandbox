require 'spec_helper'

describe AjaxController do

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      expect(response).to be_success
    end

    it "render ajax/index" do
      get 'index'
      expect(response).to render_template("ajax/index")
    end

    it "assigns count to @count" do
      session[:count] = 10
      get 'index'
      expect(assigns(:count)).to eq(session[:count])
    end

    context "when new session" do
      it "assigns count to 0" do
        session[:count] = nil
        get 'index'
        expect(assigns(:count)).to eq(0)
      end
    end
  end

  describe "GET 'counter'" do
    it "returns http success" do
      xhr :get, :counter
      expect(response).to be_success
    end

    it "responds with JSON" do
      xhr :get, :counter
      expect(response.header['Content-Type']).to include("application/json")
      expect(lambda{ JSON.parse(response.body) }).to_not raise_error
    end

    it "count up" do
      count = session[:count] = 10
      xhr :get, :counter
      expect(session[:count]).to eq(count+1)

      parsed_body = JSON.parse(response.body)
      expect(parsed_body["count"]).to eq(count+1)
    end
  end

end
