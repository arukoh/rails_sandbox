require 'spec_helper'

describe "ajax/index.html.erb" do
  it "displays counter link" do
    render
    expect(rendered).to have_selector('a', :id => "counter-link") do |element|
      expect(element.attribute("href").value).to eq('/ajax/counter')
      expect(element.attribute("data-remote").value).to eq('true')
      expect(element).to contain("counter")
    end
  end

  it "displays current count" do
    assign(:count, 1)
    render
    expect(rendered).to have_selector('div', :id => "counter") do |element|
      expect(element).to contain("1")
    end
  end
end
